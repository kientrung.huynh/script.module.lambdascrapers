# -*- coding: UTF-8 -*-
#######################################################################
 # ----------------------------------------------------------------------------
 # "THE BEER-WARE LICENSE" (Revision 42):
 # @Daddy_Blamo wrote this file.  As long as you retain this notice you
 # can do whatever you want with this stuff. If we meet some day, and you think
 # this stuff is worth it, you can buy me a beer in return. - Muad'Dib
 # ----------------------------------------------------------------------------
#######################################################################

# Addon Name: Placenta
# Addon id: plugin.video.placenta
# Addon Provider: Mr.Blamo

# Scraper Checked and Fixed 11-08-2018 -JewBMX

import re, urlparse, urllib, base64, requests
from bs4 import BeautifulSoup
from resources.lib.modules import cleantitle
from resources.lib.modules import client
from resources.lib.modules import cache
from resources.lib.modules import dom_parser2
from resources.lib.modules import debrid
from resources.lib.modules import log_utils

class source:
    def __init__(self):
        log_utils.log('============ THUVIENAZ ============')
        self.priority = 1
        self.language = ['en']
        self.domains = ['www.thuvienaz.net','fshare.vn', 'www.fshare.vn']
        self.base_link = 'https://www.thuvienaz.net/'
        self.base_search_link = 'https://www.google.com/'
        self.search_link = 'https://www.google.com/search?q=site:www.thuvienaz.net+%s' + '&ie=utf-8&oe=utf-8'
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.5',
            'Accept-Encoding': 'gzip, deflate',
            'DNT': '1',
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1'
        }


    def movie(self, imdb, title, localtitle, aliases, year):
        log_utils.log('thuvienaz - movie title ' + title + ' : ' + year)
        try:
            clean_title = cleantitle.geturl(title)

            s = requests.Session()
            url = self.search_link % clean_title
            log_utils.log('thuvienaz - search ' + url)

            r = s.get(url, headers=self.headers)
            log_utils.log(r)

            soup = BeautifulSoup(r.text, "html.parser")
            output = []
            for searchWrapper in soup.find_all('div', {'class': 'r'}):  # this line may change in future based on google's web page structure
                url = searchWrapper.find('a')["href"]
                text = searchWrapper.find('a').text.strip()
                result = {'text': text, 'url': url}
                output.append(result)

            log_utils.log(output[0])

            url = output[0]['url']
            log_utils.log('thuvienaz - goto url' + url)
            return url
        except Exception:
            log_utils('Cant found url ' + url)
            return

    def tvshow(self, imdb, tvdb, tvshowtitle, localtvshowtitle, aliases, year):
        log_utils.log('thuvienaz - goto tvshow ')
        try:
            url = {'imdb': imdb, 'tvdb': tvdb, 'tvshowtitle': tvshowtitle, 'year': year}
            url = urllib.urlencode(url)
            return url
        except:
            return

    def episode(self, url, imdb, tvdb, title, premiered, season, episode):
        log_utils.log('thuvienaz - goto episode ')
        try:
            if url == None: return

            url = urlparse.parse_qs(url)
            url = dict([(i, url[i][0]) if url[i] else (i, '') for i in url])
            url['premiered'], url['season'], url['episode'] = premiered, season, episode
            try:
                clean_title = cleantitle.geturl(url['tvshowtitle'])+'-season-%d' % int(season)
                search_url = urlparse.urljoin(self.base_link, self.search_link % clean_title.replace('-', '+'))
                r = cache.get(client.request, 1, search_url)
                r = client.parseDOM(r, 'div', {'id': 'movie-featured'})
                r = [(client.parseDOM(i, 'a', ret='href'),
                      re.findall('<b><i>(.+?)</i>', i)) for i in r]
                r = [(i[0][0], i[1][0]) for i in r if
                     cleantitle.get(i[1][0]) == cleantitle.get(clean_title)]
                url = r[0][0]
            except:
                pass
            data = client.request(url)
            data = client.parseDOM(data, 'div', attrs={'id': 'details'})
            data = zip(client.parseDOM(data, 'a'), client.parseDOM(data, 'a', ret='href'))
            url = [(i[0], i[1]) for i in data if i[0] == str(int(episode))]

            return url[0][1]
        except:
            return

    def sources(self, url, hostDict, hostprDict):
        log_utils.log('thuvienaz - goto sources ' + url)
        sources = []

        requestInt = requests.Session()
        response = requestInt.get(url, headers=self.headers)
        log_utils.log(response)

        soup = BeautifulSoup(response.text, "html.parser")
        downloadLink = soup.find('a', {'id': 'download-button'})["href"]
        log_utils.log('download link: ' + downloadLink)

        if downloadLink:
            responseLinks = requestInt.get(downloadLink, headers=self.headers)
            soup = BeautifulSoup(responseLinks.text, "html.parser")
            for row in soup.find('table', {'class': 'post_table'}).find('tbody').find_all('tr'):
                log_utils.log('received data ')
                cells = row.find_all("td")

                sourceURL = cells[3].find('a', {'class': 'face-button'})["href"]
                sources.append({
                    'source': 'fshare.vn',
                    'quality': cells[0].get_text(),
                    'language': 'en',
                    'url': sourceURL,
                    'direct': True,
                    'debridonly': False
                })
                continue
            log_utils.log(sources)
        return sources

    def resolve(self, url):
        log_utils.log('thuvienaz - goto resolve ' + url)
        if self.base_link in url:
            url = client.request(url)
            v = re.findall('document.write\(Base64.decode\("(.+?)"\)', url)[0]
            b64 = base64.b64decode(v)
            url = client.parseDOM(b64, 'iframe', ret='src')[0]
        return url
